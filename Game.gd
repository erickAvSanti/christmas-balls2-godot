extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var ChristmasBall = preload("res://ChristmasBall.tscn")
export(PackedScene) var Bullet

var touch_pressed: bool = false
var touch_pressed0: bool = false
var touch_pressed1: bool = false
var touch_pressed2: bool = false

var touch_pos = Vector2.ZERO
var touch0_pos = Vector2.ZERO
var touch1_pos = Vector2.ZERO
var touch2_pos = Vector2.ZERO

var bullets = []

var viewport_rect: Rect2

# Called when the node enters the scene tree for the first time.
func _ready():
	get_tree().get_root().connect("size_changed", self, "size_changed")
	size_changed()
func size_changed():
	
	viewport_rect = get_viewport_rect()
	$LeftRightSideLimit/Left.position.x = -30
	$LeftRightSideLimit/Left.position.y = viewport_rect.end.y/2
	
	$LeftRightSideLimit/Right.position.x = viewport_rect.end.x + 30
	$LeftRightSideLimit/Right.position.y = viewport_rect.end.y/2
	
	$TopBottomSideLimit/Top.position.y = -30 * 21
	$TopBottomSideLimit/Top.position.x = viewport_rect.end.x/2
	
	$TopBottomSideLimit/Bottom.position.y = viewport_rect.end.y + 30
	$TopBottomSideLimit/Bottom.position.x = viewport_rect.end.x/2
	
	$Player.position.x = viewport_rect.end.x / 2
	$Player.position.y = viewport_rect.end.y - $Player/player2.texture.get_size().y / 2
	$PositionFireChristmasBall.position.x = viewport_rect.end.x / 2
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _on_Timer_timeout():
	var pressed = Input.is_action_pressed("mouse_left")
	if pressed:
		 create_bullet_at(get_global_mouse_position())
	if touch_pressed0:
		 create_bullet_at(touch0_pos)
	if touch_pressed1:
		 create_bullet_at(touch1_pos)
	if touch_pressed2:
		 create_bullet_at(touch2_pos)
		

func create_bullet_at(pos) -> void:
	var tmp_pos = Vector2(pos.x,pos.y)
	var b = Bullet.instance()
	b.position.x = $Player.position.x
	b.position.y = $Player.position.y 
	add_child(b)
	var vel = (tmp_pos - b.position).normalized()
	b.direction = vel
	tmp_pos = Vector2(vel.x,vel.y)
	b.add_force(Vector2.ZERO,tmp_pos * 90)
	b.look_at(pos)
	#b.add_force(Vector2.ZERO,mouse_pos)
	
	




func _on_TimerFireChristmasBall_timeout():
	var ball = ChristmasBall.instance()
	ball.position = $PositionFireChristmasBall.position
	ball.get_node("AnimatedSprite").frame = randi() % 4
	add_child(ball)
	
func _input(event):
	if event is InputEventScreenTouch :
		if event.index == 0:
			touch_pressed0 = event.is_pressed()
			touch0_pos = event.position
		if event.index == 1:
			touch_pressed1 = event.is_pressed()
			touch1_pos = event.position
		if event.index == 2:
			touch_pressed2 = event.is_pressed()
			touch2_pos = event.position
	
	if event is InputEventScreenDrag :
		if event.index == 0:
			touch0_pos = event.position
		if event.index == 1:
			touch1_pos = event.position
		if event.index == 2:
			touch2_pos = event.position
	
